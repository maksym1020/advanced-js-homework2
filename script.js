"use strict"

const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70,
        // article: 234234
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70,
        // article: 35345
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70,
        // article: 35345
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40,
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    },
];

const root = document.querySelector('#root');

class BooksList {
    constructor(books) {
        this.books = books;
    }

    render(filteredBooks) {

        const fragment = document.createDocumentFragment();
        const list = document.createElement('ul');

        filteredBooks.forEach((element)=>{

            let listItem = document.createElement('li');

            Object.keys(element).forEach((key)=>{
                const paragraph = document.createElement('p');
                paragraph.textContent = `${key}: ${element[key]}`
                listItem.append(paragraph);
            })

            fragment.append(listItem);
        })
        list.append(fragment);
        root.append(list);
    }

    isValid() {

        // Пошук всіх унікальних властивостей в массиві
        let fields = {};
        this.books.forEach((element) => {
            fields = {...fields, ...element};
        })

        // Пошук всіх відсутніх властивостей об'єкту та вивід у консоль виключення

        this.books.forEach((element) => {
            Object.keys(fields).forEach((value) => {
                try {
                    if (!Object.keys(element).includes(value)) {

                        // Маркування об'єкту з виключеннями
                        element.isValid = 'false';

                        throw Error(`${value} відсутнє в книзі ${element.name}`);
                    }
                } catch (e) {
                    console.error(`${e.name}: ${e.message}`);
                }
            })
        })

        // Створення відфільтрованого масиву зі всіма потрібними властивостями
        let filteredBooks = [];

        this.books.forEach((element)=>{
            if(element.isValid !== 'false'){
                filteredBooks.push(element);
            }
        })

        return filteredBooks;
    }
}

const booksList = new BooksList(books);
booksList.render(booksList.isValid());